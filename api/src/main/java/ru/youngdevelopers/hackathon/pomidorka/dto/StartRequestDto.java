package ru.youngdevelopers.hackathon.pomidorka.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marnikitta on 29.10.16.
 */
public class StartRequestDto {
    private final long clientTs;

    private final long startTs;

    private final long stopTs;

    private final TypeDto type;

    @JsonCreator
    public StartRequestDto(final @JsonProperty("clientTs") long clientTs,
                           final @JsonProperty("startTs") long startTs,
                           final @JsonProperty("stopTs") long stopTs,
                           final @JsonProperty("type") TypeDto type) {
        this.clientTs = clientTs;
        this.startTs = startTs;
        this.stopTs = stopTs;
        this.type = type;
    }

    public long getClientTs() {
        return clientTs;
    }

    public long getStartTs() {
        return startTs;
    }

    public long getStopTs() {
        return stopTs;
    }

    public TypeDto getType() {
        return type;
    }
}
