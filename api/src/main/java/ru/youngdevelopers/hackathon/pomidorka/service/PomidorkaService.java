package ru.youngdevelopers.hackathon.pomidorka.service;

import ru.youngdevelopers.hackathon.pomidorka.dto.PomidorkaDto;
import ru.youngdevelopers.hackathon.pomidorka.dto.StartRequestDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by marnikitta on 29.10.16.
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("pomidorka")
public interface PomidorkaService {

    @POST
    @Path("/start")
    @Consumes(MediaType.APPLICATION_JSON)
    PomidorkaDto start(StartRequestDto pomidorka);

    @GET
    @Path("/status")
    PomidorkaDto current();

    @POST
    @Path("/abort")
    PomidorkaDto abort(String id);
}
