package ru.youngdevelopers.hackathon.pomidorka.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by marnikitta on 28.10.16.
 */
@Produces("text/html")
public interface PingService {

    @GET
    @Path("ping")
    String ping();
}
