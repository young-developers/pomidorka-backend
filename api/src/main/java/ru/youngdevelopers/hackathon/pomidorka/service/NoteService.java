package ru.youngdevelopers.hackathon.pomidorka.service;

import ru.youngdevelopers.hackathon.pomidorka.dto.NoteDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by marnikitta on 29.10.16.
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("note")
public interface NoteService {
    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    NoteDto createNote(@FormParam("payload") String payload);

    @GET
    List<NoteDto> notes();

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("update")
    NoteDto update(@FormParam("id") String id,
                   @FormParam("payload") String payload,
                   @FormParam("isCompleted") boolean isCompleted);
}
