package ru.youngdevelopers.hackathon.pomidorka.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marnikitta on 29.10.16.
 */
public class PomidorkaDto {
    private final String id;

    private final long serverTs;

    private final long startTs;

    private final long stopTs;

    private final TypeDto type;

    private final StatusDto status;

    @JsonCreator
    public PomidorkaDto(final @JsonProperty(value = "id", defaultValue = "null") String id,
                        final @JsonProperty(value = "serverTs", defaultValue = "0") long serverTs,
                        final @JsonProperty(value = "startTs", defaultValue = "0") long startTs,
                        final @JsonProperty(value = "stopTs", defaultValue = "0") long stopTs,
                        final @JsonProperty(value = "type", defaultValue = "null") TypeDto type,
                        final @JsonProperty(value = "status", defaultValue = "null") StatusDto status) {
        this.id = id;
        this.startTs = startTs;
        this.stopTs = stopTs;
        this.type = type;
        this.status = status;
        this.serverTs = serverTs;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("serverTs")
    public long getServerTs() {
        return serverTs;
    }

    @JsonProperty("startTs")
    public long getStartTs() {
        return startTs;
    }

    @JsonProperty("stopTs")
    public long getStopTs() {
        return stopTs;
    }

    @JsonProperty("type")
    public TypeDto getType() {
        return type;
    }

    @JsonProperty("status")
    public StatusDto getStatus() {
        return status;
    }
}

