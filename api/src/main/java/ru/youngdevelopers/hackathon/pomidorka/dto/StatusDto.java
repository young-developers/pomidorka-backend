package ru.youngdevelopers.hackathon.pomidorka.dto;

/**
 * Created by marnikitta on 29.10.16.
 */
public enum StatusDto {
    RUNNING,
    ABORTED,
    FINISHED
}
