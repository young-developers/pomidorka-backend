package ru.youngdevelopers.hackathon.pomidorka.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marnikitta on 29.10.16.
 */
public class NoteDto {
    private final String id;

    private final boolean isCompleted;

    private final long createTs;

    private final long lastUpdateTs;

    private final String payload;

    @JsonCreator
    public NoteDto(final @JsonProperty("id") String id,
                   final @JsonProperty("isCompleted") boolean isCompleted,
                   final @JsonProperty("createdTs") long createTs,
                   final @JsonProperty("lastUpdateTs") long lastUpdateTs,
                   final @JsonProperty("payload") String payload) {
        this.id = id;
        this.isCompleted = isCompleted;
        this.createTs = createTs;
        this.lastUpdateTs = lastUpdateTs;
        this.payload = payload;
    }

    public String getId() {
        return id;
    }

    @JsonProperty("isCompleted")
    public boolean isCompleted() {
        return isCompleted;
    }

    public long getCreateTs() {
        return createTs;
    }

    public long getLastUpdateTs() {
        return lastUpdateTs;
    }

    public String getPayload() {
        return payload;
    }
}
