package ru.youngdevelopers.hackathon.pomidorka.ws.service.impl;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Pomidorka;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Status;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Type;
import ru.youngdevelopers.hackathon.pomidorka.domain.repository.PomidorkaRepository;
import ru.youngdevelopers.hackathon.pomidorka.domain.service.PomidorkaLifecycleService;
import ru.youngdevelopers.hackathon.pomidorka.dto.PomidorkaDto;
import ru.youngdevelopers.hackathon.pomidorka.dto.StartRequestDto;
import ru.youngdevelopers.hackathon.pomidorka.dto.StatusDto;
import ru.youngdevelopers.hackathon.pomidorka.dto.TypeDto;
import ru.youngdevelopers.hackathon.pomidorka.service.PomidorkaService;

import javax.inject.Inject;
import java.util.UUID;

/**
 * Created by marnikitta on 29.10.16.
 */
@Service("pomidorkaService")
public class PomidorkaServiceImpl implements PomidorkaService {

    @Inject
    private PomidorkaLifecycleService pomidorkaLifecycleService;

    @Inject
    private PomidorkaRepository repository;

    private final static String userId = "user";

    private final static Pomidorka defaultPomidorka = new Pomidorka(UUID.randomUUID().toString(), userId,
            DateTime.now(), DateTime.now(), Type.REST, Status.FINISHED);

    @Override
    public PomidorkaDto start(final StartRequestDto pomidorka) {
        return toDto(pomidorkaLifecycleService.start(
                userId,
                new DateTime(pomidorka.getStartTs()),
                new DateTime(pomidorka.getStopTs()),
                Type.valueOf(pomidorka.getType().name())
        ));
    }

    private PomidorkaDto toDto(final Pomidorka dto) {
        return new PomidorkaDto(
                dto.getId(),
                DateTime.now().getMillis(),
                dto.getStartTs().getMillis(),
                dto.getStopTs().getMillis(),
                TypeDto.valueOf(dto.getType().name()),
                StatusDto.valueOf(dto.getStatus().name())
        );
    }

    @Override
    public PomidorkaDto current() {
//        return toDto(repository.latestPomidorka(userId).orElse(defaultPomidorka));
        return toDto(pomidorkaLifecycleService.updated(repository.latestPomidorka(userId).orElse(defaultPomidorka).getId()));
    }

    @Override
    public PomidorkaDto abort(final String id) {
        return toDto(pomidorkaLifecycleService.abort(id));
    }
}
