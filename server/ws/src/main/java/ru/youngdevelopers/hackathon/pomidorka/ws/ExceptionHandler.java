package ru.youngdevelopers.hackathon.pomidorka.ws;

import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by marnikitta on 29.10.16.
 */

@Component
public class ExceptionHandler implements ExceptionMapper<Exception> {
    public Response toResponse(Exception exception) {
        Response.Status status;

        status = Response.Status.INTERNAL_SERVER_ERROR;

        return Response.status(status).header("exception", exception.getMessage()).build();
    }
}
