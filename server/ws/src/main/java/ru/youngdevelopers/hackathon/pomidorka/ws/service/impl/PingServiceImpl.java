package ru.youngdevelopers.hackathon.pomidorka.ws.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.youngdevelopers.hackathon.pomidorka.service.PingService;

/**
 * Created by marnikitta on 28.10.16.
 */
@Service("pingService")
public class PingServiceImpl implements PingService {

    @Value("${aba}")
    private String aba;

    public String ping() {
        return aba;
    }
}
