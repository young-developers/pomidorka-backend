package ru.youngdevelopers.hackathon.pomidorka.ws.service.impl;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Note;
import ru.youngdevelopers.hackathon.pomidorka.domain.repository.NoteRepository;
import ru.youngdevelopers.hackathon.pomidorka.dto.NoteDto;
import ru.youngdevelopers.hackathon.pomidorka.service.NoteService;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by marnikitta on 30.10.16.
 */
@Service("noteService")
public class NoteServiceImpl implements NoteService {
    @Inject
    private NoteRepository noteRepository;

    private final static String userId = "user4";

    @Override
    public NoteDto createNote(final String payload) {
        return toDto(noteRepository.create(new Note(
                UUID.randomUUID().toString(),
                userId,
                false,
                new DateTime(),
                new DateTime(),
                payload
        )));
    }

    private NoteDto toDto(final Note note) {
        return new NoteDto(
                note.getId(),
                note.isCompleted(),
                note.getCreateTs().getMillis(),
                note.getLastUpdateTs().getMillis(),
                note.getPayload()
        );
    }

    @Override
    public List<NoteDto> notes() {
//        return Collections.singletonList(
//                new NoteDto(UUID.randomUUID().toString(),
//                        false,
//                        new DateTime().getMillis(),
//                        new DateTime().getMillis(),
//                        "Abacaba"));
        return noteRepository.notes(userId).stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public NoteDto update(final String id, final String payload, final boolean isCompleted) {
        return toDto(noteRepository.update(id, payload, isCompleted));
    }
}
