/**
 * This class is generated by jOOQ
 */
package ru.youngdevelopers.hackathon.pomidorka.domain.db;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;

import ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.Note;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.Pomidorka;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.SchemaVersion;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Public extends SchemaImpl {

    private static final long serialVersionUID = -1173928835;

    /**
     * The reference instance of <code>PUBLIC</code>
     */
    public static final Public PUBLIC = new Public();

    /**
     * The table <code>PUBLIC.schema_version</code>.
     */
    public final SchemaVersion SCHEMA_VERSION = ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.SchemaVersion.SCHEMA_VERSION;

    /**
     * The table <code>PUBLIC.POMIDORKA</code>.
     */
    public final Pomidorka POMIDORKA = ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.Pomidorka.POMIDORKA;

    /**
     * The table <code>PUBLIC.NOTE</code>.
     */
    public final Note NOTE = ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.Note.NOTE;

    /**
     * No further instances allowed
     */
    private Public() {
        super("PUBLIC", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            SchemaVersion.SCHEMA_VERSION,
            Pomidorka.POMIDORKA,
            Note.NOTE);
    }
}
