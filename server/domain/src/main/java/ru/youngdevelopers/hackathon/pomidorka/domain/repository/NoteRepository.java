package ru.youngdevelopers.hackathon.pomidorka.domain.repository;

import ru.youngdevelopers.hackathon.pomidorka.domain.model.Note;

import java.util.List;

/**
 * Created by marnikitta on 30.10.16.
 */
public interface NoteRepository {
    Note create(Note note);

    List<Note> notes(String userId);

    Note update(String id, String payload, boolean isCompleted);
}
