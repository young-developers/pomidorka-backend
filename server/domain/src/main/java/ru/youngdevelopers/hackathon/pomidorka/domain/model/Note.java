package ru.youngdevelopers.hackathon.pomidorka.domain.model;

import org.joda.time.DateTime;

/**
 * Created by marnikitta on 30.10.16.
 */
public class Note {
    private final String id;

    private final String owner;

    private final boolean isCompleted;

    private final DateTime createTs;

    private final DateTime lastUpdateTs;

    private final String payload;

    public Note(final String id,
                final String owner,
                final boolean isCompleted,
                final DateTime createTs,
                final DateTime lastUpdateTs,
                final String payload) {
        this.id = id;
        this.owner = owner;
        this.isCompleted = isCompleted;
        this.createTs = createTs;
        this.lastUpdateTs = lastUpdateTs;
        this.payload = payload;
    }

    public String getOwner() {
        return owner;
    }

    public String getId() {
        return id;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public DateTime getCreateTs() {
        return createTs;
    }

    public DateTime getLastUpdateTs() {
        return lastUpdateTs;
    }

    public String getPayload() {
        return payload;
    }
}
