package ru.youngdevelopers.hackathon.pomidorka.domain.service.impl;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Pomidorka;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Status;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Type;
import ru.youngdevelopers.hackathon.pomidorka.domain.repository.PomidorkaRepository;
import ru.youngdevelopers.hackathon.pomidorka.domain.service.PomidorkaLifecycleService;

import javax.inject.Inject;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by marnikitta on 29.10.16.
 */
@Service
public class PomidorkaLifecycleServiceImpl implements PomidorkaLifecycleService {

    @Inject
    private PomidorkaRepository pomidorkaRepository;

    @Override
    @Transactional
    public Pomidorka start(final String userId, final DateTime startTs, final DateTime stopTs, final Type type) {
        Optional<Pomidorka> current = pomidorkaRepository.latestPomidorka(userId).map(p -> updated(p.getId()));
        if (current.map(p -> p.getStatus() == Status.RUNNING && p.getType() == type).orElse(false)) {
            return current.get();
        } else {
            Pomidorka pomidorka = new Pomidorka(UUID.randomUUID().toString(), userId, startTs, stopTs, type, Status.RUNNING);
            return pomidorkaRepository.create(pomidorka);
        }
    }

    @Override
    public Pomidorka updated(final String pomidorkaId) {
        Pomidorka pomidorka = pomidorkaRepository.loadOrException(pomidorkaId);
        if (pomidorka.getStatus() == Status.RUNNING && pomidorka.isCompleted()) {
            return pomidorkaRepository.updateStatus(pomidorkaId, Status.FINISHED);
        }
        return pomidorka;
    }

    @Override
    public Pomidorka abort(final String pomidorkaId) {
        return pomidorkaRepository.updateStatus(pomidorkaId, Status.ABORTED);
    }
}
