package ru.youngdevelopers.hackathon.pomidorka.domain.repository.impl;

import org.joda.time.DateTime;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.Tables;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.records.NoteRecord;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Note;
import ru.youngdevelopers.hackathon.pomidorka.domain.repository.NoteRepository;

import javax.inject.Inject;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by marnikitta on 30.10.16.
 */
@Repository
@Transactional
public class DbNoteRepository implements NoteRepository {

    @Inject
    private DSLContext sql;

    @Override
    public Note create(final Note note) {
        sql.insertInto(Tables.NOTE).set(toRecord(note)).execute();
        return loadOrException(note.getId());
    }

    public Optional<Note> load(String id) {
        return sql.fetch(Tables.NOTE, Tables.NOTE.ID.eq(id)).stream().map(this::fromRecord).findAny();
    }

    public Note loadOrException(String id) {
        return load(id).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public List<Note> notes(final String userId) {
        return sql.fetch(Tables.NOTE, Tables.NOTE.OWNER.eq(userId)).stream().map(this::fromRecord)
                .collect(Collectors.toList());
    }

    @Override
    public Note update(final String id, final String payload, final boolean isCompleted) {
        final int rows = sql.update(Tables.NOTE).set(Tables.NOTE.PAYLOAD, payload).set(Tables.NOTE.IS_COMPLETED, isCompleted)
                .where(Tables.NOTE.ID.eq(id)).execute();
        return loadOrException(id);
    }

    private NoteRecord toRecord(final Note note) {
        return new NoteRecord(
                note.getId(),
                note.getOwner(),
                note.isCompleted(),
                note.getCreateTs().getMillis(),
                note.getLastUpdateTs().getMillis(),
                note.getPayload()
        );
    }

    private Note fromRecord(final NoteRecord record) {
        return new Note(
                record.getId(),
                record.getOwner(),
                record.getIsCompleted(),
                new DateTime(record.getCreatedTs()),
                new DateTime(record.getLastUpdatTs()),
                record.getPayload()
        );
    }
}
