package ru.youngdevelopers.hackathon.pomidorka.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

/**
 * Created by marnikitta on 29.10.16.
 */
public class Pomidorka {
    private final String id;

    private final String owner;

    private final DateTime startTs;

    private final DateTime stopTs;

    private final Type type;

    private final Status status;

    public Pomidorka(final String id,
                     final String owner,
                     final DateTime startTs,
                     final DateTime stopTs,
                     final Type type,
                     final Status status) {
        this.id = id;
        this.owner = owner;
        this.startTs = startTs;
        this.stopTs = stopTs;
        this.type = type;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public DateTime getStartTs() {
        return startTs;
    }

    public DateTime getStopTs() {
        return stopTs;
    }

    public Type getType() {
        return type;
    }

    public Status getStatus() {
        return status;
    }

    public boolean isCompleted() {
        return DateTime.now().isAfter(stopTs);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Pomidorka pomidorka = (Pomidorka) o;

        return new EqualsBuilder()
                .append(id, pomidorka.id)
                .append(owner, pomidorka.owner)
                .append(startTs, pomidorka.startTs)
                .append(stopTs, pomidorka.stopTs)
                .append(type, pomidorka.type)
                .append(status, pomidorka.status)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(owner)
                .append(startTs)
                .append(stopTs)
                .append(type)
                .append(status)
                .toHashCode();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Pomidorka{");
        sb.append("id='").append(id).append('\'');
        sb.append(", owner='").append(owner).append('\'');
        sb.append(", startTs=").append(startTs);
        sb.append(", stopTs=").append(stopTs);
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
