package ru.youngdevelopers.hackathon.pomidorka.domain.service;

import org.joda.time.DateTime;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Pomidorka;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Type;

/**
 * Created by marnikitta on 29.10.16.
 */
public interface PomidorkaLifecycleService {
    Pomidorka start(String userId, DateTime startTs, DateTime stopTs, Type type);

    Pomidorka updated(String pomidorkaId);

    Pomidorka abort(String pomidorkaId);
}
