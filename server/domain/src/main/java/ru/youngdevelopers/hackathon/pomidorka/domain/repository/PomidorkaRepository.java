package ru.youngdevelopers.hackathon.pomidorka.domain.repository;

import ru.youngdevelopers.hackathon.pomidorka.domain.model.Pomidorka;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Status;

import java.util.Optional;

/**
 * Created by marnikitta on 29.10.16.
 */
public interface PomidorkaRepository {
    Pomidorka create(Pomidorka pomidorka);

    Optional<Pomidorka> latestPomidorka(String userId);

    Pomidorka updateStatus(String id, Status status);

    Pomidorka loadOrException(String id);

    Optional<Pomidorka> load(String id);
}
