/**
 * This class is generated by jOOQ
 */
package ru.youngdevelopers.hackathon.pomidorka.domain.db.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;

import ru.youngdevelopers.hackathon.pomidorka.domain.db.Keys;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.Public;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.records.PomidorkaRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Pomidorka extends TableImpl<PomidorkaRecord> {

    private static final long serialVersionUID = 1060933479;

    /**
     * The reference instance of <code>PUBLIC.POMIDORKA</code>
     */
    public static final Pomidorka POMIDORKA = new Pomidorka();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PomidorkaRecord> getRecordType() {
        return PomidorkaRecord.class;
    }

    /**
     * The column <code>PUBLIC.POMIDORKA.ID</code>.
     */
    public final TableField<PomidorkaRecord, String> ID = createField("ID", org.jooq.impl.SQLDataType.VARCHAR.length(128).nullable(false), this, "");

    /**
     * The column <code>PUBLIC.POMIDORKA.OWNER_ID</code>.
     */
    public final TableField<PomidorkaRecord, String> OWNER_ID = createField("OWNER_ID", org.jooq.impl.SQLDataType.VARCHAR.length(128).nullable(false), this, "");

    /**
     * The column <code>PUBLIC.POMIDORKA.START_TS</code>.
     */
    public final TableField<PomidorkaRecord, Long> START_TS = createField("START_TS", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>PUBLIC.POMIDORKA.STOP_TS</code>.
     */
    public final TableField<PomidorkaRecord, Long> STOP_TS = createField("STOP_TS", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>PUBLIC.POMIDORKA.TYPE</code>.
     */
    public final TableField<PomidorkaRecord, String> TYPE = createField("TYPE", org.jooq.impl.SQLDataType.VARCHAR.length(64).nullable(false), this, "");

    /**
     * The column <code>PUBLIC.POMIDORKA.STATUS</code>.
     */
    public final TableField<PomidorkaRecord, String> STATUS = createField("STATUS", org.jooq.impl.SQLDataType.VARCHAR.length(64).nullable(false), this, "");

    /**
     * Create a <code>PUBLIC.POMIDORKA</code> table reference
     */
    public Pomidorka() {
        this("POMIDORKA", null);
    }

    /**
     * Create an aliased <code>PUBLIC.POMIDORKA</code> table reference
     */
    public Pomidorka(String alias) {
        this(alias, POMIDORKA);
    }

    private Pomidorka(String alias, Table<PomidorkaRecord> aliased) {
        this(alias, aliased, null);
    }

    private Pomidorka(String alias, Table<PomidorkaRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<PomidorkaRecord> getPrimaryKey() {
        return Keys.CONSTRAINT_C;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<PomidorkaRecord>> getKeys() {
        return Arrays.<UniqueKey<PomidorkaRecord>>asList(Keys.CONSTRAINT_C);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pomidorka as(String alias) {
        return new Pomidorka(alias, this);
    }

    /**
     * Rename this table
     */
    public Pomidorka rename(String name) {
        return new Pomidorka(name, null);
    }
}
