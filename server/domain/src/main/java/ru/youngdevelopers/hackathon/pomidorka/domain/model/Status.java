package ru.youngdevelopers.hackathon.pomidorka.domain.model;

/**
 * Created by marnikitta on 29.10.16.
 */
public enum Status {
    RUNNING,
    FINISHED,
    ABORTED
}
