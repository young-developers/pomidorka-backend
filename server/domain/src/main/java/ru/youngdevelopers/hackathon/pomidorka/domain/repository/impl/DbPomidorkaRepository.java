package ru.youngdevelopers.hackathon.pomidorka.domain.repository.impl;

import org.joda.time.DateTime;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.Tables;
import ru.youngdevelopers.hackathon.pomidorka.domain.db.tables.records.PomidorkaRecord;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Pomidorka;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Status;
import ru.youngdevelopers.hackathon.pomidorka.domain.model.Type;
import ru.youngdevelopers.hackathon.pomidorka.domain.repository.PomidorkaRepository;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by marnikitta on 29.10.16.
 */
@Repository
public class DbPomidorkaRepository implements PomidorkaRepository {

    @Inject
    private DSLContext sql;

    @Override
    public Pomidorka create(final Pomidorka pomidorka) {
        int rows = sql.insertInto(Tables.POMIDORKA).set(toRecord(pomidorka)).execute();
        if (rows >= 1) {
            return pomidorka;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public Optional<Pomidorka> latestPomidorka(final String userId) {
        return sql.select().from(Tables.POMIDORKA).where(Tables.POMIDORKA.OWNER_ID.eq(userId)).orderBy(Tables.POMIDORKA.START_TS.desc()).limit(1).fetchInto(Tables.POMIDORKA)
                .stream().findFirst().map(this::fromRecord);
    }

    @Override
    public Pomidorka updateStatus(final String id, final Status status) {
        sql.update(Tables.POMIDORKA).set(Tables.POMIDORKA.STATUS, status.name())
                .where(Tables.POMIDORKA.ID.eq(id))
                .execute();
        return loadOrException(id);
    }

    @Override
    public Pomidorka loadOrException(final String id) {
        return load(id).orElseThrow(() -> new IllegalArgumentException("No such pomidorka"));
    }

    @Override
    public Optional<Pomidorka> load(final String id) {
        return sql.fetch(Tables.POMIDORKA, Tables.POMIDORKA.ID.eq(id)).stream().map(this::fromRecord).findAny();
    }

    private PomidorkaRecord toRecord(final Pomidorka pomidorka) {
        return new PomidorkaRecord(
                pomidorka.getId(),
                pomidorka.getOwner(),
                pomidorka.getStartTs().getMillis(),
                pomidorka.getStopTs().getMillis(),
                pomidorka.getType().name(),
                pomidorka.getStatus().name()
        );
    }

    private Pomidorka fromRecord(final PomidorkaRecord record) {
        return new Pomidorka(
                record.getId(),
                record.getOwnerId(),
                new DateTime(record.getStartTs()),
                new DateTime(record.getStopTs()),
                Type.valueOf(record.getType()),
                Status.valueOf(record.getStatus())
        );
    }
}
