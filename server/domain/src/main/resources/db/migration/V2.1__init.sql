CREATE TABLE pomidorka (
  id       VARCHAR(128) PRIMARY KEY,
  owner_id VARCHAR(128) NOT NULL,
  start_ts BIGINT       NOT NULL,
  stop_ts  BIGINT       NOT NULL,
  type     VARCHAR(64)  NOT NULL,
  status   VARCHAR(64)  NOT NULL
);
INSERT INTO pomidorka (id, owner_id, start_ts, stop_ts, type, status)
VALUES ('123e4567-e89b-12d3-a456-426655440000', 'user', '123', '321', 'REST', 'FINISHED');

