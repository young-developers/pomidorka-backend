CREATE TABLE note (
  id            VARCHAR(128) PRIMARY KEY,
  owner         VARCHAR(128) NOT NULL,
  is_completed BOOL NOT NULL DEFAULT FALSE,
  created_ts    BIGINT       NOT NULL,
  last_updat_ts BIGINT       NOT NULL,
  payload       TEXT         NOT NULL
)